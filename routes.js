var express=require('express');
var app=express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())


app.use('/api/tab1'           , require('./server/api/tab1'));
app.use('/api/tab2'           , require('./server/api/tab2'));
  
var port = 9000;
var hostname = 'localhost';

app.listen(port);

console.log('Server running on ', hostname + ":" + port);