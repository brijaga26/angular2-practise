var express = require('express');
var router = express.Router();
var cors = require('cors');

var data = [
  {i:1, value:20},
  {i:2, value:20},
  {i:3, value:20}
];

router.use(cors());

router
.get('/',function(req,res){
  res.json(data)
});

module.exports = router;