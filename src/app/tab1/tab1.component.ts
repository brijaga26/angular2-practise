import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HttpClient } from '@angular/common/http';

const tab_Url = 'http://localhost:9000/api/tab1';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./tab1.component.css'],
  providers: [HttpClient],
  encapsulation: ViewEncapsulation.None
})
export class Tab1Component implements OnInit {

  tab_Result: any;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get(tab_Url).subscribe(data => {
      this.tab_Result = data;
    });
  }
}
