import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Tab1Component } from '../tab1/tab1.component';
import { Tab2Component } from '../tab2/tab2.component';

const appRoutes: Routes = [

{ path: 'tab1', component: Tab1Component },
{ path: 'tab2', component: Tab2Component },

];

@NgModule({
  imports: [
    RouterModule.forRoot( appRoutes )
  ],
  exports: [
    RouterModule
  ]
})
export class TabRoutingModule {}
