import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsComponent } from './tabs.component';
import { Tab1Component } from '../tab1/tab1.component';
import { Tab2Component } from '../tab2/tab2.component';

import { TabRoutingModule } from './tab-router.module';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    TabRoutingModule,
    HttpClientModule
  ],
  exports: [
    TabsComponent
  ],
  declarations: [
    TabsComponent,
    Tab1Component,
    Tab2Component
  ]
})
export class TabsModule { }
